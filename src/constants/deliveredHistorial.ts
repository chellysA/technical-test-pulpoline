import { IProduct } from "../types/product";
import order1 from "../assets/img/Order1.png";

const deliveredHistorial: IProduct[] = [
  {
    id: 1,
    img: order1,
    description: "Sambal Fried Fish with Fresh Vegetables",
  },
];

export default deliveredHistorial;
