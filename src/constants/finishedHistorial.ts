import { IProduct } from "../types/product";
import order1 from "../assets/img/Order1.png";
import order2 from "../assets/img/Order2.png";

const finishedHistorial: IProduct[] = [
  {
    id: 1,
    img: order1,
    description: "Sambal Fried Fish with Fresh Vegetables",
  },
  {
    id: 2,
    img: order2,
    description: "Archipelago Noodles with Chicken Katsu",
  },
];

export default finishedHistorial;
