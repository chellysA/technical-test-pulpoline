import category1 from "../assets/img/Category1.png";
import category2 from "../assets/img/Category2.png";
import category3 from "../assets/img/Category3.png";
import categoryMobile1 from "../assets/img/Category1Mobile.png";
import categoryMobile2 from "../assets/img/Category2Mobile.png";
import categoryMobile3 from "../assets/img/Category3Mobile.png";

interface ICategories {
  name: string;
  image: string;
  mobileImg: string;
}

export const categories: ICategories[] = [
  {
    name: "Indonesia Food",
    image: category1,
    mobileImg: categoryMobile1,
  },
  {
    name: "Japanese Food",
    image: category2,
    mobileImg: categoryMobile2,
  },
  {
    name: "Korean Food",
    image: category3,
    mobileImg: categoryMobile3,
  },
];
