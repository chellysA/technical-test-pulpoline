# Landing Page para prueba técnica Front-end PulpoLine
Este proyecto tiene como objetivo demostrar las habilidades en el desarrollo de interfaces front end.

# Lenguaje e implementaciones:
Se ha implementado el lenguaje React, se creó un proyecto componetizado y ordenado en carpetas para su mejor y facil lectura, usando tipado de typescript.
La librerias; Data Aos, Tailwind y slick-carousel han sido utitlizadas para el uso del css y estilos varios.

# Funcionalidades
En varios de sus componentes se han aplicado funcionalidades pertinentes a la lógica del diseño y la pagina, usando hooks de React para su funcionamiento.

# Preview del proyecto

```
npm install
npm build
npm run preview
```

# React + TypeScript + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

Currently, two official plugins are available:

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) uses [Babel](https://babeljs.io/) for Fast Refresh
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) uses [SWC](https://swc.rs/) for Fast Refresh

## Expanding the ESLint configuration

If you are developing a production application, we recommend updating the configuration to enable type aware lint rules:

- Configure the top-level `parserOptions` property like this:

```js
export default {
  // other rules...
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    project: ['./tsconfig.json', './tsconfig.node.json'],
    tsconfigRootDir: __dirname,
  },
}
```

- Replace `plugin:@typescript-eslint/recommended` to `plugin:@typescript-eslint/recommended-type-checked` or `plugin:@typescript-eslint/strict-type-checked`
- Optionally add `plugin:@typescript-eslint/stylistic-type-checked`
- Install [eslint-plugin-react](https://github.com/jsx-eslint/eslint-plugin-react) and add `plugin:react/recommended` & `plugin:react/jsx-runtime` to the `extends` list
